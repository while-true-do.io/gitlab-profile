<div align="center">

<img alt="logo" src="docs/assets/logo/favicon.png">

# while-true-do.io

[![Blog](https://img.shields.io/static/v1?style=flat-square&label=Blog&message=blog.while-true-do.io)](https://blog.while-true-do.io)
[![Matrix](https://img.shields.io/matrix/whiletruedoio-community:matrix.org?style=flat-square&label=matrix&logo=matrix)](https://matrix.to/#/#whiletruedoio-community:matrix.org)

</div>

Hi, and welcome to [while-true-do.io](https://while-true-do.io). This small
introduction should give you an idea of the purpose and goals of the projects,
hosted in the group.

## :muscle: Motivation

while-true-do.io was founded to have a hosting space for a small community of
developers and enthusiasts. One might say "a space to share thoughts".
Unfortunately, this never really took off and I (Daniel Schier) am the only
permanent contributor. Anyway, I discovered that I love to write articles,
come up with some code for the public and share my thoughts with you. Today,
the intention of this blog is "share my thoughts with the community".

## :hammer: Work

Most of the content you can find in the group is made to make
[while-true-do.io](https://while-true-do.io) possible. Therefore, you may find
our automation, themes, content for some articles or containers here.

I aim eager to make the content and code usable in arbitrary infrastructure, so
you can copy-paste or use the releases directly. But, there is no guarantee that
it works.

## :pencil: Contribute

In case you want to contribute in any way, I would be happy to receive
whatever you want to offer. Please feel free to check out the
[contribution guidelines](docs/CONTRIBUTING.md) and open issues, request
features or open merge/pull requests.

## :e-mail: Contact

In case you want to get in touch with me or reach out for general questions,
please use one of the below contact details

- Blog: [blog.while-true-do.io](https://blog.while-true-do.io)
- Code: [gitlab.com/while-true-do.io](https://gitlab.com/whiletruedoio)
- Chat: [#whiletruedoio-community](https://matrix.to/#/#whiletruedoio-community:matrix.org)
- Mail: [hello@while-true-do.io](mailto:hello@while-true-do.io)
